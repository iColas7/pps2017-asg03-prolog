% inv(List,List)
% example: inv([1,2,3],[3,2,1]).

inv(L,R):- accInv(L,[],R).
accInv([H|T],A,R):- accInv(T,[H|A],R). 
accInv([],A,A).


% Per poter invertire l'ordine di una lista si puo' usare un accumulatore.
% Passo base: se una lista e' vuota, dato un elemento, questo verra' aggiunto alla lista.
% Ricorsione: della lista iniziale prendo la testa e la metto come testa nell'accumulatore,
% poi chiamo ricorsivamente lo stesso predicato passando come lista iniziale la coda (quindi ho tolto la testa), come lista finale
% l'accumulatore (che adesso conterra' la testa della lista iniziale) e la lista finale. 