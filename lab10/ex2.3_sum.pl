% sum(List,Sum)
sum([], 0).
sum([H|T], Sum) :- sum(T, Rest), Sum is H + Rest.

% Caso base: lista vuota, somma zero.
% Ricorsione: tutte le volte metto al posto della testa la coda, poi imposto la Sum come la somma della testa precedente e Rest (variabile temporanea)

% Esempio
% Goal: sum([10,20,30],X).
% Primo passo: H = 10, T = 20, 30. Chiamo sum e metto come testa 20,30, in Rest invece H + Rest (10 + 0)
% Secondo passo: H = 20, T = 30. Chiamo sum e metto come testa 30, in Rest invece H + Rest (20 + 10)
% Terzo passo: H = 30, T = nil. Chiamo sum che però non farà nulla, in Rest invece H + Rest (30 + 30)