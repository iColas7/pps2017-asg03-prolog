% same(List1,List2)
% are the two lists the same?
same([],[]).
same([X|Xs],[X|Ys]):- same(Xs,Ys).

% Passo base: se le due liste sono vuote, allora sono uguali.
% Ricorsione: si analizzano le due teste delle due liste: se sono uguali, richiamo la same sulle code. Andra' avanti fino
% a quando avremo liste vuote (quindi saranno uguali) oppure quando una testa differira' dall'altra.