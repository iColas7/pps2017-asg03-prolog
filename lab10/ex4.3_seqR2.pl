% seqR2(N,List)
% example: seqR2(4,[0,1,2,3,4]).

seqR2(X,L) :- seqR3(0,X,L).
seqR3(X,X,[X]) :- !.
seqR3(X,Y,[X|Xs]) :- X =< Y, Z is X+1, seqR3(Z,Y,Xs).

% Chiamo il predicato seqR2 passando come termini un valore ed una lista. Da qui chiamo seqR3, cui aggiungo come primo termine lo 0 per potermi costruire un range che va da 0 al valore inserito.
% Passo base: quando trovo il valore inserito mi fermo.
% Ricorsione: parto dal valore minimo del range, quindi 0; lo aggiungo alla testa della lista finale, poi controllo che 0 sia <= valore (nel mio caso 4), imposto Z = X+1 (quindi 0+1) e se e' cosi'
% richiamo il predicato passando (1,4,codaListaFinale).  Cosi' via fin quando non raggiungo l'ultimo elemento, che corrisponde al valore inserito come primo termine nella seqR2. 