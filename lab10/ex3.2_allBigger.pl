% all_bigger(List1,List2)
% all elements in List1 are bigger than those in List2, 1 by 1 
% example: all_bigger([10,20,30,40],[9,19,29,39]).

all_bigger([],[]).
all_bigger([X|Xs],[Y|Ys]):- all_bigger(Xs,Ys), X > Y.

% Passo base: se due liste sono vuote, allora va bene.
% Ricorsione: date due liste, prendo le rispettive teste e verifico che la testa della prima lista sia > della testa della seconda.
% Se cosi' fosse, chiamo ancora la funzione passando pero' solo le code.