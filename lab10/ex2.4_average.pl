% average(List,Average)
% it uses average(List,Count,Sum,Average)
average(List,A) :- average(List,0,0,A). 
average([],C,S,A) :- A is S/C. 
average([X|Xs],C,S,A) :- C2 is C+1, S2 is S+X, average(Xs,C2,S2,A).

% Il primo predicato serve per inizializzare il tutto: data una lista e una variabile A, si impostano i contatori C (count) e S (sum) a 0.
% Il secondo predicato e' l'ultimo che verra' chiamato, ovvero quando la lista e' vuota. Per trovare A (average): S/C
% Il terzo predicato e' quello che verra' chiamato ricorsivamente: ad ogni iterazione la testa verra' sommata ad S e verra' incrementato 
% di 1 il contatore, fino a quando non si svuotera' la lista.