% seq(N,List)
% example: seq(5,[0,0,0,0,0]).
seq(0,[]).
seq(N,[0|T]):- N > 0, N2 is N-1, seq(N2,T).


% Chiamo ricorsivamente seq N-1 volte fino a quando N e' > 0, e ad ogni iterazione lo decremento. In questo caso voglio usare una lista di 0,
% ma potrei sostituire la testa 0 in seq con qualunque altra cosa.