% search_two(Elem,List)
% looks for two occurrences of Elem with an element in between!
search2(X,[X,_,X|_]). 
search2(X,[_|Xs]):-search2(X,Xs).

% Cerca due elementi uguali con un altro elemento nel mezzo.
% Passo base: controlla se nella testa c'e' il valore x, poi un qualsiasi elemento, e successivamente un altro x.
% Ricorsione: se nella testa cosi' non fosse, richiama il predicato sulla coda.