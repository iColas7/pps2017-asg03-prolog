% double(List,List)
% suggestion: remember predicate append/3 
% example: double([1,2,3],[1,2,3,1,2,3]).

double(L,R):- doubleTemp(L,L,R).
doubleTemp([],L,L).
doubleTemp([X|Xs],L,[X|M]):- doubleTemp(Xs,L,M).

% Considerando la append/3, mi sono reso conto che servivano assolutamente tre terms per poter gestire questo problema.
% Per cui nel momento in cui viene invocato il predicato double, chiamo un altro predicato (doubleTemp) che duplichi la lista iniziale per non perdere l'ordine e i valori in essa contenuti.
% A questo punto aggiungo la testa della lista iniziale alla nuova lista, e una volta fatto cio' per tutti i valori, ovvero fino
% a quando la lista non diventa vuota, aggiungo alla nuova lista la lista artificiale che mi ero creato per mantenere tutti i valori dell'iniziale.