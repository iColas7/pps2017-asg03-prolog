% max(List,Max)
% Max is the biggest element in List
% Suppose the list has at least one element

max([X],X). 
max([H|T], Y):- max(T,Y), H < Y.
max([H|T], H):- max(T,Y), H >= Y.

% Passo base: se ho un solo elemento nella lista sarà lui il massimo.

% Esempio
% Goal: max([10,210,30],X).
% Prima iterazione: ancora non ho un max, quindi parto dal secondo predicato. Richiamo max sulla coda (210,30) mettendo H = 10.
% Seconda iterazione: adesso ho che la testa (210) e' maggiore di H impostato prima, quindi richiamo il secondo fatto perche' H >= Y (210 > 10).
% Terza iterazione: adesso ho che la testa (30) e' minore di H impostato prima, quindi il max e' 210.