% sublist(List1,List2)
% List1 should be a subset of List2 
% example: sublist([1,2],[5,3,2,1]).

search(X,[X|_]). 
search(X,[_|Xs]):-search(X,Xs).
sublist([],Y).
sublist([X|Xs],Y) :- search(X,Y), sublist(Xs,Y).

% Passo base: se ho la prima lista vuota, quella sara' contenuta nella seconda lista indipendentemente dagli elementi contenuti.
% Ricorsione: prendo il primo elemento (testa) della prima lista, e chiamo la search su quell'elemento. Poi chiamo la sublist sulla coda
% (quindi su tutti gli elementi della prima lista ad eccezione della testa appena analizzata).

% Esempio:
% sublist([1,2],[5,3,2,1]).
% Vado alla ricorsione. Prendo la testa della prima lista (1) e chiamo la search su 1 e la seconda lista. Poi chiamo la sublist su (2) e la seconda lista.
% Se 1 non e' contenuto nella seconda lista, ho finito. Altrimenti procedo in questo modo, quindi prendo la testa della prima lista (2)
% e chiamo la search su 2 e sulla seconda lista. In questo caso se lo trova allora ho finito perche' la prima lista non ha piu' elementi.