% seqR(N,List)
% example: seqR(4,[4,3,2,1,0]).

% Due possibili soluzioni.

% Prima soluzione: uso del cut. Vado avanti fin quando l'ultimo elemento e' 0, a quel punto vado al passo base e mi fermo grazie al cut.
seqR(0,[0]):- !.
seqR(N,[N|T]):- N > 0, N2 is N-1, seqR(N2,T).

% Seconda soluzione: non uso il cut. Quindi andro' avanti con la ricorsione anche dopo l'ultimo elemento (0), quindi il passo base
% si deve aspettare un -1 e una lista vuota.
seqR_1(-1,[]).
seqR_1(N,[N|T]):- N >= 0, N2 is N-1, seqR_1(N2,T).
