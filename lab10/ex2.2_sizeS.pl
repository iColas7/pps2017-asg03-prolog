% size(List,Size)
% Size will contain the number of elements in List, written using notation zero, s(zero), s(s(zero))..
size([], zero).
size([_|T],s(M)) :- size(T,M).

% Ad ogni iterazione verra' eliminata la testa della lista e verra' aggiunto un s(). Nel momento in cui la lista sara'
% vuota significa che abbiamo terminato e torneremo al passo base, dove verra' aggiunto 'zero' nelle ultime parentesi aperte.