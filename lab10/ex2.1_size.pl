% size(List,Size)
% Size will contain the number of elements in List
size([],0).
size([_|T],M) :- size(T,N), M is N+1.

% Ogni volta viene richiamata la size sulla lista senza però comprendere la testa.
% Passo base: se la lista e' vuota, allora la sua dimensione e' 0.
% Ricorsione: elimino la testa ed incremento la dimensione della lista, poi richiamo la size sulla coda.