% search2(Elem,List)
% looks for two consecutive occurrences of Elem
search2(X,[X,X|_]). 
search2(X,[_|Xs]):-search2(X,Xs).

% Cerca due elementi consecutivi all'interno della lista.
% Passo base: controlla se i due elementi consecutivi sono nella testa della lista.
% Ricorsione: se non sono nella testa, richiama il predicato sulla coda.