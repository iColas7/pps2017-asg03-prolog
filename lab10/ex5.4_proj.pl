% proj(List,List)
% example: proj([[1,2],[3,4],[5,6]],[1,3,5]).

proj([],[]).
proj([[H|_]|T],[H|T2]) :- proj(T,T2).

% Caso base: due liste vuote.
% Ricorsione: prendo in esame la prima sottolista, nella fattispecie la sua testa. La aggiungo alla seconda lista, poi richiamo proj sulla coda della prima lista 
% che dunque prendera' in considerazione tutte le sottoliste ad eccezione di quella appena analizzata, e la nuova lista.

% Esempio.
% proj([[1,2],[3,4],[5,6]],L).
% Nel passo ricorsivo avremo che la testa della prima sottolista e' 1, mentre la coda sara' composta da [[3,4],[5,6]]. Aggiungo 1 alla seconda lista e richiamo 
% proj escludendo la prima sottolista, ovvero passando solo T e T2.