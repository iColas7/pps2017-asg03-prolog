% search(Elem,List)
search(X,[X|_]). 
search(X,[_|Xs]):-search(X,Xs).

% Cerca un elemento data una lista.
% Passo base: controlla se l'elemento da cercare e' nella testa.
% Ricorsione: se l'elemento non e' nella testa, richiama il predicato sulla coda.