% search_anytwo(Elem,List)
% looks for any Elem that occurs two times
search(X,[X|_]). 
search(X,[_|Xs]):- search(X,Xs).

% Prendo il primo elemento della lista (testa).
% Poi con la search ciclo sulla coda (Xs) e cerco quell'elemento.
search_anytwo(X,[X|Xs]):- search(X,Xs).
search_anytwo(X,[_|Xs]):- search_anytwo(X,Xs).

% Esempio
% Goal: search_anytwo(a,[b,c,a,d,e,a,d,e]).
% Alla prima iterazione, capito nel secondo fatto di search_anytwo perchè la testa (b) != a, quindi richiamo search_anytwo eliminando la testa (b).
% Alla seconda iterazione idem perche' c != a.
% Alla terza iterazione trovo finalmente a = a, quindi rientro nel primo fatto che richiama la search, ovvero adesso cerco nella coda se c'e' un'altra a.
% La search procede fino a quando non trova una a nella coda.