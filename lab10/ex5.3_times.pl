% times(List,N,List)
% example: times([1,2,3],3,[1,2,3,1,2,3,1,2,3]).

% Prima soluzione
% Predicato per poter fare un inserimento in coda.
tail_insert([],E,[E]).
tail_insert([H|T],E,[H|T2]) :- tail_insert(T,E,T2).

concat(L,[],L).
concat(L,[H|T],L0) :- tail_insert(L,H,L2), concat(L2,T,L0).

% Predicato che si ripete N volte per poter richiamare la concat N volte.
times(L, 1, L) :- !.
times(L, N, L0) :- N2 is N-1, times(L, N2, L2), concat(L, L2, L0).



% Seconda soluzione
times1(L, 1, L) :- !.
times1(L, N, L0) :- M is N-1, times1(L, M, L1), append(L, L1, L0).

