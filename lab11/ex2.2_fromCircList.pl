% fromCircList(+List,-Graph) 
% fromCircList1(+List,-Graph,@Head)

fromCircList([H|T],G) :- fromCircList1([H|T],G,H).
fromCircList1([L],[e(L,H)], H).
fromCircList1([H1,H2|T],[e(H1,H2)|L],H) :- fromCircList1([H2|T],L,H).

% Qui vogliamo realizzare una lista circolare. L'unica differenza rispetto a prima: l'ultimo passaggio prevede che la coda si colleghi alla testa.
% Per fare cio' abbiamo bisogno di due predicati distinti: fromCircList e fromCircList1.

% fromCircList viene chiamato all'inizio e richiama il secondo predicato, aggiungendo come terzo terms la testa della lista.
% fromCircList1 si comporta come il predicato dell'esercizio precedente tenendo pero' traccia della testa, la quale e' l'ultimo collegamento da fare.

% Passo base: se ho rimasto un elemento nella lista, allora aggancio l'elemento che mi arriva dalla precedente iterazione (coda) alla testa che ho portato avanti e termino.
% Ricorsione: creo di volta in volta i nodi eliminando il primo elemento della lista, richiamando lo stesso predicato passando la coda.

% Esempio
% fromCircList([1,2,3],X). Solution: fromCircList([1,2,3],[e(1,2),e(2,3),e(3,1)]).
% Alla prima iterazione si chiama fromCircList1([1,2,3],X,1).
% Alla seconda iterazione, si creera' il nodo e(1,2), poi si richiama il predicato passando il secondo elemento (2) e il resto della coda.
% Alla terza iterazione, si creera' il nodo e(2,3), poi si richiama il predicato passando il secondo elemento (3) e il resto della coda.
% A questo punto la coda e' vuota, quindi si va al passo base dove si aggancia la coda alla testa ottenendo e(3,1).