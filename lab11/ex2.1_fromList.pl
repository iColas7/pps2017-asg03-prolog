% fromList(+List,-Graph)
fromList([_],[]).
fromList([H1,H2|T],[e(H1,H2)|L]) :- fromList([H2|T],L).

% Creazione di un grafo data una lista.

% Passo base: quando ho rimasto un elemento, ho terminato.
% Ricorsione: suddiviso la lista in testa e coda, dove nella testa ci sono due elementi e nella coda i rimanenti.
% A questo punto prelevo la testa e creo il collegamento, poi richiamo il predicato passando solo il secondo elemento e la coda.
% All'iterazione successiva si fara' la stessa cosa, ovvero si divide la testa in due (secondo elemento e terzo elemento) e si crea il collegamento.