% dropFirst: drops only the first occurrence (showing no alternative results)
% dropFirst(Elem,List,OutList)

dropFirst(X,[X|T],T) :- !. 
dropFirst(X,[H|Xs],[H|L]) :- dropFirst(X,Xs,L).

% Vogliamo cancellare solamente la prima occorrenza.
% E' esattamente come dropAny, ma con l'aggiunta del cut che permette di bloccare tutto non appena si cancella un'occorrenza.