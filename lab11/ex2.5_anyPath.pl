% anypath(+Graph, +Node1, +Node2, -ListPath)
% a path from Node1 to Node2
% if there are many path, they are showed 1-by-1

% Suggestions.
% 1) A path from N1 to N2 exists if there is a e(N1,N2)
% 2) A path from N1 to N2 is OK if N3 can be reached from N1, and then there is a path from N2 to N3, recursively.

anyPath(G, N1, N2, [e(N1, N2)]) :- member(e(N1, N2), G).
anyPath(G, N1, N2, [e(N1, N3)|L]) :- member(e(N1, N3), G), anyPath(G, N3, N2, L).

% Il primo predicato si occupa di capire (fa fede al suggerimento 1) se la coppia di nodi da noi indicata effettivamente esiste nel grafo. 
% Questo controllo e' possibile grazie al predicato member, che prende in input come primo term una coppia di nodi e come secondo il grafo sul quale verificare la presenza del collegamento tra due nodi.
% Se la risposta e' positiva, significa che c'e' un collegamento diretto tra i due nodi da noi indicati e il percorso esiste (e questo e' uno dei possibili, potrebbe anche essere l'unico).

% Il secondo predicato si occupa di capire (fa fede al suggerimento 2) se esistono altri percorsi per raggiungere il nodo2 partendo dal nodo1.
% Utilizziamo ancora la member, passando N1 e N3 (uno dei nodi possibili presenti nel grafo) e verificando che sia presente nel grafo come coppia di nodi. Se il risultato e' OK, richiamiamo il predicato passando ancora N3 e N2, per verificare se esiste questo percorso.
% Se anche questa risposta sara' positiva, abbiamo trovato un nuovo percorso.

% Esempio
% anyPath([e(1,2),e(1,3),e(2,3)],1,3,L). First solution: anyPath([e(1,2),e(1,3),e(2,3)],1,3,[e(1,3)]). Second solution: Solution: anyPath([e(1,2),e(1,3),e(2,3)],1,3,[e(1,2),e(2,3)]).

% Alla prima iterazione verifico sempre che esista un collegamento diretto tra N1 e N2, quindi entro nel primo predicato e chiamo la member.
% Se quel collegamento esiste, allora la prima soluzione sara' proprio data da e(N1,N2), altrimenti mi fermo.
% Alla seconda iterazione si analizza la prima coppia di nodi, che necessariamente dovra' avere come primo elemento N1. In questo caso abbiamo (1,2) e va bene, il 2 diventa N3 e verifico che esista una coppia di nodi (1,2) nel grafo.
% Se quel collegamento esiste, allora chiamo ricorsivamente il predicato passando come nodi N2 e N3 per vedere se anche loro sono collegati. Cosi' via fin quando non raggiungo N2, allora quello e' un percorso ammissibile.