% dropNode(+Graph, +Node, -OutGraph)
% drop all edges starting and leaving from a Node 
% use dropAll defined in 1.1
:-include('ex1.4_dropAll.pl').

dropNode(G,N,O):- dropAll(G,e(N,_),G2), dropAll(G2,e(_,N),O).
