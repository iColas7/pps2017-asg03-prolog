% dropAll: drop all occurrences, returning a single list as result
% dropAll(Elem,List,OutList)

dropAll(X, [], []).
dropAll(X,[X|T],L) :- !, dropAll(X,T,L).
dropAll(X,[Y|T],[Y|L]) :- dropAll(X,T,L).

% Qui vogliamo cancellare in un colpo solo tutte le occorrenze di una lista.

% Passo base: se la lista iniziale e' vuota, ho finito.
% Ricorsione: bisogna distinguere due casi. 
% 1) Se il primo elemento della lista e' uguale al valore che voglio cancellare, allora lo elimino richiamando il predicato sulla coda.
% 2) Se il primo elemento della lista e' diverso dal valore che voglio cancellare, allora lo aggiungo alla lista di output e richiamo il predicato sulla coda.

% Esempio
% dropAll(10,[10,20,10,30],X).
% Alla prima iterazione, cado nel secondo predicato, che mi cancella la testa (10) e richiama se stesso sulla coda (quindi dropAll(10,[20,10,30],X)).
% Alla seconda iterazione, cado nel terzo predicato, che aggiunge la testa (20) alla lista di output e richiama se stesso sulla coda (quindi dropAll(10,[10,30],[20])).
% Alla terza iterazione, cado nel secondo predicato, che mi cancella la testa (10) e richiama se stesso sulla coda (quindi dropAll(10,[30],[20])).
% Alla quarta iterazione, cado nel terzo predicato, che aggiunge la testa (30) alla lista di output e richiama se stesso (dropAll(10,[],[20,30])).
% La lista iniziale e' vuota, quindi cado nel passo base e termino.





