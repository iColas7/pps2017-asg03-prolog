% reaching(+Graph, +Node, -List)
% all the nodes that can be reached in 1 step from Node 
% possibly use findall, looking for e(Node,_) combined 
% with member(?Elem,?List)

reaching([], N, []).
reaching([e(N, N1)|T], N, [N1|L]) :- reaching(T, N, L), !.
reaching([H|T], N, L) :- reaching(T, N, L).

% Dato un grafo e selezionato un nodo, vogliamo scoprire quali nodi raggiungiamo in un solo passo partendo dal nodo indicato.

% Passo base: se il grafo e' terminato (lista vuota), finisco (uso del cut per non proseguire).
% Ricorsione: si divide in due casi:
% 1) Se il primo elemento del nodo analizzato e' lo stesso nodo scelto, allora lo aggiungo alla lista di output e richiamo il predicato passando la coda del grafo (quindi escludo i due elementi appena analizzati).
% 2) Se invece il primo elemento non e' uguale al nodo scelto, entro nel terzo predicato che semplicemente elimina la testa del grafo e poi richiama se stesso passando la coda.


% Esempio
% reaching([e(1,2),e(1,2),e(2,3)],1,L). Solution: reaching([e(1,2),e(1,2),e(2,3)],1,[2,2]).
% Alla prima iterazione, entra nel secondo predicato perche' il primo elemento della testa (1) e' uguale al nodo scelto. Aggiunge dunque il secondo elemento (2, che e' quello che raggiunge in un passo) alla lista di output e richiama se stesso senza la prima coppia di nodi.
% Alla seconda iterazione, entra ancora nel secondo predicato, perche' il primo elemento della seconda coppia di nodi (1) e' uguale al nodo scelto. Aggiunge dunque il secondo elemento della testa (3) alla lista di output e richiama se stesso senza la prima coppia di nodi.
% Alla terza iterazione, entra nel terzo predicato perche' il nodo scelto (1) e' diverso dal primo elemento della testa (2). Quindi richiama se stesso senza la testa.
% Alla quarta iterazione, entra ancora nel terzo predicato, dove dovra' eliminare la testa (3) perche' diversa dal nodo scelto.
% A questo punto il grafo iniziale e' vuoto, abbiamo concluso. Il risultato, ovvero i nodi raggiunti dal nodo scelto in un passo sono contenuti nel terzo term, ovvero la lista.
