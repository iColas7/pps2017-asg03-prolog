% dropLast: drops only the last occurrence (showing no alternative results)
% dropLast(Elem,List,OutList)

dropLast(X,[X|T],T):- !.
dropLast(X,[H|Xs],[H|L]):- reverse([X|T],Z), dropLast(X,Z,L).

% Adesso vogliamo cancellare l'ultima occorrenza della lista.
% Per semplicita' si puo' fare il reverse della lista originale e dunque cancellare la prima occorrenza.

% Passo base: se l'elemento da cancellare e' nella testa, lo cancello e mi fermo con il cut.
% Ricorsione: al primo giro faccio il reverse della lista salvando la nuova lista nella variabile Z, poi richiamo il predicato passando la lista ribaltata. 

