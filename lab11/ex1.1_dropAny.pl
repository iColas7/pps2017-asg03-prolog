% dropAny(?Elem,?List,?OutList)
dropAny(X,[X|T],T). 
dropAny(X,[H|Xs],[H|L]) :- dropAny(X,Xs,L).

% Cancella tutte le occorrenze dell'elemento scelto dalla lista, pero' uno alla volta. 
% Quindi alla fine ci saranno N soluzioni, dove N e' il numero delle occorrenze di quell'elemento.

% Passo base: se l'elemento da cancellare e' nella testa, torno la coda.
% Ricorsione: se l'elemento da cancellare non e' nella testa, richiamo il predicato sulla coda.