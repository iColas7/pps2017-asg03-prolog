% allReaching(+Graph, +Node, -List)
% all the nodes that can be reached from Node 
% Suppose the graph is NOT circular!
% Use findall and anyPath!
:-include('ex2.5_anyPath.pl').

allReaching(G, N1, L) :- findall(N2, anyPath(G, N1, N2, L1), L).

% Vogliamo, dato un nodo di partenza, trovare tutti i nodi che si raggiungono partendo dal nodo iniziale.

% Significato del predicato built-in findall/3 (arita' 3, quindi avra' tre parametri). 
% Essa mi permette di dare un goal (anyPath) e se e' soddisfatto inserire all'interno della lista (L) tutti i risultati trovati, utilizzando tra i termini del goal anche una variabile (N2).

% Esempio 
% allReaching([e(1,2),e(2,3),e(3,5)],1,X). Solution: allReaching([e(1,2),e(2,3),e(3,5)],1,[2,3,5]).
% Semplicemente succede che, una volta chiamato il predicato allReaching, esso si affidera' alla findall per richiamare la anyPath passando il nodo scelto e N2, 
% dove N2 rappresenta tutti i possibili nodi presenti nel grafo. La computazione rimane la stessa dell'esercizio precedente, alla fine il risultato sara' inserito in L.
% Primo passo: la anyPath entrera' nel primo predicato solo con N2 = 2, che e' l'unico collegamento diretto che ha N1.
% Altri passi: ricorsivamente andra' a cercare i nodi attaccati a N2 = 2 trovando ad esempio N3 = 3, poi cerchera' quelli attaccati a N3 = 3 e cosi' via.